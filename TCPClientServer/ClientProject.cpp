/*
	Nama	: M Arya Rakha P;
	Kelas	: 1 D4 Teknologi Game;
	NRP     : 4210171023
	Script  : Client
*/


#include "stdafx.h"
#include <WS2tcpip.h>
#include <iostream>
#include <string>
using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main() {
	//Variabel result, penyimpan return nilai saat memanggil fungsi
	int result;

	//Variabel tempat mengisi data, ketika memanggil fungsi WSAStartup()
	WSAData wsaData;

	//Variabel string menyimpan alamat ip server
	string IP_SERVER = "127.0.0.1";

	//Variabel int penyimpan port yang akan digunakan
	int DEFAULT_PORT = 5555;

	//Menginisialisasi winsock
	cout << "Initializing winsock..." << endl;
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);

	//Jika gagal menginisialisasi winsock
	if (result != 0) {
		cout << "Error initializing: " << WSAGetLastError() << endl;
		return 1;
	}

	//Membuat variabel hint penyimpan informasi jaringan (jenis ip, port, address)
	sockaddr_in hints;

	//Mengeset IPv4
	hints.sin_family = AF_INET;

	//Mengset port
	hints.sin_port = htons(DEFAULT_PORT);

	//Mengset informasi address ke variabel hints
	inet_pton(AF_INET, IP_SERVER.c_str(), &hints.sin_addr);

	//Membuat socket untuk client
	cout << "Creating socket..." << endl;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	//Jika socket gagal dibuat
	if (sock == INVALID_SOCKET) {
		cout << "Failed to create socket..." << endl;
		WSACleanup();
		return 1;
	}

	//Melakukan koneksi dengan server
	result = connect(sock, (SOCKADDR *)&hints, sizeof(hints));

	//Jika gagal mengkoniksikan dengan server
	if (result != 0) {
		cout << "Failed connecting to server..." << endl;
		WSACleanup();
		return 1;
	}

	cout << "Connected to server..." << endl << endl;

	string text;
	cout << "Type -1 to disconnect" << endl;

	//Looping untuk mengirim pesan ke servver
	do {
		cout << "Masukkan text: ";
		cin >> text;

		//Mengirim data yang telah dimasukkan ke server
		send(sock, text.c_str(), 512, 0);
	}
	while (text != "-1");

	//Menutup socket client
	closesocket(sock);

	//Menghapus winsock
	WSACleanup();

	return 0;
}