﻿/*
	Nama	: Darmawan Saptura;
	Kelas	: 1 D4 Teknologi Game;
	NRP     : 4210171001
	Script  : Server
*/

#include "stdafx.h"
#include <WS2tcpip.h>
#include <iostream>
using namespace std;

#pragma comment(lib, "Ws2_32.lib")

int main() {
	//Variabel result, penyimpan return nilai saat memanggil fungsi
	int result;

	//Variabel tempat mengisi data, ketika memanggil fungsi WSAStartup()
	WSAData wsaData;
	
	//Variabel int penyimpan port yang akan digunakan
	int DEFAULT_PORT = 5555;

	//Menginisialisasi winsock
	cout << "Initializing winsock..." << endl;
	result = WSAStartup(MAKEWORD(2, 2), &wsaData);

	//Jika gagal menginisialisasi winsock
	if (result != 0) {
		cout << "Error initializing: " << WSAGetLastError() << endl;
		return 1;
	}

	//Membuat variabel hint penyimpan informasi jaringan (jenis ip, port, address)
	sockaddr_in hints;

	//Mengeset IPv4
	hints.sin_family = AF_INET;

	//Mengset port
	hints.sin_port = htons(DEFAULT_PORT);

	//Mengset INADDR_ANY untuk server
	hints.sin_addr.S_un.S_addr = INADDR_ANY;

	//Membuat socket untuk server
	cout << "Creating socket..." << endl;
	SOCKET sock = socket(AF_INET, SOCK_STREAM, 0);

	//Jika socket gagal dibuat
	if (sock == INVALID_SOCKET) {
		cout << "Failed to create socket..." << endl;
		WSACleanup();
		return 1;
	}

	//Membinding socket dengan informasi jaringan yang telah dibuat
	result = bind(sock, (SOCKADDR *)&hints, sizeof(hints));

	//Mengecek jika socket error
	if (result != 0) {
		cout << "Failed to binding socket..." << endl;
		WSACleanup();
		return 1;
	}

	cout << "Server up..." << endl;
	cout << "Waiting client..." << endl;

	//Membuka akses untuk client yang ingin melakukan kontak
	result = listen(sock, SOMAXCONN);

	//Jika gagal melisten jaringan
	if (result != 0) {
		cout << "Failed to listening socket..." << endl;
		WSACleanup();
		return 1;
	}

	//Jika ada client yang terkoneksi, maka terima socket client dan masukkan ke variabel socket client
	SOCKET client = accept(sock, NULL, NULL);

	//Jika socket client invalid
	if (client == INVALID_SOCKET) {
		cout << "Failed to accept client socket..." << endl;
		WSACleanup();
		return 1;
	}

	cout << "Client connected...\n\n";

	char buf[512];

	//Proses menerima dan menampilkan data yang dikirim oleh client
	do {
		//Menerima data dari client
		result = recv(client, buf, 512, 0);

		//Jika client mengirim informasi data, maka tampilkan data
		if (result > 0) {
			cout << "Data diterima: " << buf << endl;
		}

		//Jika client mengirim informasi disconnect
		else if (result == 0) {
			cout << "Client closing..." << endl;
		}
	}
	while (result > 0);

	//Menutup socket server
	closesocket(sock);

	//Menutup socket client
	closesocket(client);

	//Menghapus winsock
	WSACleanup();

	return 0;
}